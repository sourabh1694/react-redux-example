export const CREATE_ORDER_STEP1 = 'CREATE_ORDER_STEP1';
export const CREATE_ORDER_STEP2 = 'CREATE_ORDER_STEP2';
export const CREATE_ORDER_STEP3 = 'CREATE_ORDER_STEP3';
export const PREVIOUS = 'PREVIOUS';


export function createOrderStep1(values) {
  // console.log(values);
  return {
    type: CREATE_ORDER_STEP1,
    payload: values
  };
}

export function createOrderStep2(values) {
  // console.log(values);
  return {
    type: CREATE_ORDER_STEP2,
    payload: values
  };
}

export function createOrderStep3(values) {
  // console.log(values);
  return {
    type: CREATE_ORDER_STEP3,
    payload: values
  };
}

export function previous(value) {
  // console.log(value);
  return {
    type: PREVIOUS,
    payload: value
  };
}