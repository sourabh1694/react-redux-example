import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import { createOrderStep1 } from '../actions';

const renderSelectField = ({ input, label, type, meta: { touched, error }, children }) => (
  <div>
    <label>{label}</label>
    <div>
      <select {...input} className="select-area">
        {children}
      </select>
      {touched && error && <span>{error}</span>}
    </div>
  </div>
)

class FormStep1 extends Component {

  submitForm(values) {
    // event.preventDefault();
    this.props.createOrderStep1(values);
    
  }
  
  renderField(field) {
    const { meta: { touched, error } } = field;
    const className = `form-group ${touched && error ? 'has-danger': ''}`;
    return (
      <div className={className}>
        <div className="col-xs-12">
          <label>{field.label}</label>
          <input
            type = {field.type}
            min= {field.min}
            max= {field.max}
            value = {field.value}
            onChange = {field.onChange}
            className = "form-control"
            required
            {...field.input}
            />
            {touched ? error: ''}
        </div>
      </div>
    );
  }
  
  render() {
    const { handleSubmit } = this.props;
    
    // console.log('state:', this.state);
    return(
      <form onSubmit={handleSubmit(this.submitForm.bind(this))}>
        <div className="form-group">
          <label>Please Select a meal</label>
          <div>
            <Field name="meal" component={renderSelectField} required>
              <option value="">Select Option</option>
              <option value="dinner">Dinner</option>
              <option value="lunch">Lunch</option>
              <option value="breakfast">BreakFast</option>
            </Field>
          </div>
        </div>
        <div className="form-group">
          <Field
            label="Please Enter Number of people"
            name="people"
            type="number"
            component={this.renderField}
            min="1" 
            max="10" 
          />
        </div>
        <div className="d-flex flex-row-reverse">
          <button type="submit" className="btn btn-primary">Next</button>
        </div>
      </form>
    );
  }
} 

function validate(values) {
  const errors = {};

  if (!values.people){
    errors.people = "Enter a Number!";
  }

  if (!values.meal){
    errors.meal = "Please Select!";
  }
  

  return errors;
}

function mapStateToProps(state) {
  return { orders: state.orders };
}

const mapPropsToDispatch = (dispatch) => {  
  return bindActionCreators({createOrderStep1}, dispatch);
};

export default reduxForm({
  validate,
  form: 'FormStep1Form'
})(
connect(mapStateToProps, mapPropsToDispatch)(FormStep1)
); 