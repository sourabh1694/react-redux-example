import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import { createOrderStep3, previous } from '../actions';
import { FaPlus } from "react-icons/fa";
import data from '../data/dishes.json';

const renderSelectField = ({ input, label, type, meta: { touched, error }, children }) => (
  <div>
    <label>{label}</label>
    <div>
      <select {...input} className="select-area">
        {children}
      </select>
      {touched && error && <span>{error}</span>}
    </div>
  </div>
)

class FormStep3 extends Component {

  submitForm(values) {
    // event.preventDefault();
    this.props.createOrderStep3(values);
    
  }

  previousRedirect(value) {
    // console.log(value);
    this.props.previous(value);
  }
  
  renderField(field) {
    const { meta: { touched, error } } = field;
    const className = `form-group ${touched && error ? 'has-danger': ''}`;
    return (
      <div className={className}>
        <div className="col-xs-12">
          <label>{field.label}</label>
          <input
            type = {field.type}
            min= {field.min}
            max= {field.max}
            value = {field.value}
            onChange = {field.onChange}
            className = "form-control"
            required
            {...field.input}
            />
            {touched ? error: ''}
        </div>
      </div>
    );
  }
  
  render() {
    const { handleSubmit } = this.props;
    
    // console.log('state:', this.state);
    // console.log(this.props.orders);
    return(
      <form onSubmit={handleSubmit(this.submitForm.bind(this))}>
        <div className="row">
          <div className="col-6">
            <label>Please Select a Dish</label>
          </div>
          <div className="col-6">
            <label>Please Enter Number of servings</label>
          </div>
        </div>
        {this.props.orders.dishes.map((item) => {
            return(
              <div className="row" key={item.id}>
                <div className="col-6">
                  <div>{item.id}: {item.name}</div>
                </div>
                <div className="col-6">
                  <div>
                    {item.quantity}
                  </div>
                </div>
              </div>
              );
          })
        }
        <div className="row">
          <div className="col-6">
            <div className="form-group">
              <div>
                <Field name="dish" component={renderSelectField} required>
                  <option value="">Select Option</option>
                  {data.dishes.map(item => {
                    if(item.restaurant === this.props.orders.restaurant) {
                      const selectItem = item.availableMeals.map((data, index) => {
                        // console.log(data);
                        if (data === this.props.orders.meal) {
                          return (<option value={item.name} key={index}>{item.name}</option>); 
                        }
                      })
                      return selectItem;
                    }
                    
                    })
                  }
                </Field>
              </div>
            </div>
          </div>
          <div className="col-6">
            <div className="form-group">
              <Field
                label=""
                name="quantity"
                type="number"
                component={this.renderField}
                min="1" 
                max="100"
              />
            </div>
          </div>
        </div>
        <button type="submit" className="btn btn-light add"><FaPlus /></button>
        <div className="d-flex justify-content-between">
          <button type="button" className="btn btn-light" onClick={this.previousRedirect.bind(this, 'step2')}>Previous</button>
          {
            !this.props.orders.dishes.length ? 
            (
              <button type="button" className="btn btn-primary" disabled>Next</button>
            )
            : 
            (
              <button type="button" className="btn btn-primary" onClick={this.previousRedirect.bind(this, 'review')}>Next</button>   
            )
          }
        </div>
      </form>
    );
  }
} 

function validate(values) {
  const errors = {};

  if (!values.quantity){
    errors.quantity = "Enter a Number!";
  }

  if (!values.dish){
    errors.dish = "Please Select!";
  }
  

  return errors;
}

function mapStateToProps(state) {
  return { orders: state.orders };
}

const mapPropsToDispatch = (dispatch) => {  
  return bindActionCreators({createOrderStep3, previous}, dispatch);
};

export default reduxForm({
  validate,
  form: 'FormStep3Form'
})(
connect(mapStateToProps, mapPropsToDispatch)(FormStep3)
); 

