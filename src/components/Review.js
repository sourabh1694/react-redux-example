import React, { Component } from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import { previous } from '../actions';

class Review extends Component {

  previousRedirect(value) {
    // console.log(value);
    this.props.previous(value);
  }
  Capitalize(str){
    return str.charAt(0).toUpperCase() + str.slice(1);
    }
  

  render() {
    const {orders} = this.props;
    return(
      <div>
        <table className="table">
          <tbody>
            <tr>
              <th scope="row">Meal</th>
              <td>{this.Capitalize(orders.meal)}</td>
            </tr>
            <tr>
              <th scope="row">No. of People</th>
              <td>{orders.people}</td>
            </tr>
            <tr>
              <th scope="row">Restaurant</th>
              <td>{this.Capitalize(orders.restaurant)}</td>
            </tr>
            <tr>
              <th scope="row">Dishes</th>
              <td>
                {orders.dishes.map(item => {
                    return (
                      <div key={item.id}>{this.Capitalize(item.name)} - {item.quantity}</div>
                    )
                  }) 
                }
              </td>
            </tr>
          </tbody>
        </table>
        <div className="d-flex justify-content-between">
          <button type="button" className="btn btn-light" onClick={this.previousRedirect.bind(this, 'step3')}>Previous</button>
          <button type="button" className="btn btn-primary">Submit</button>
        </div>
      </div>
      );
  }
}

function mapStateToProps(state) {
  return { orders: state.orders };
}

const mapPropsToDispatch = (dispatch) => {  
  return bindActionCreators({previous}, dispatch);
};

export default connect(mapStateToProps, mapPropsToDispatch)(Review);