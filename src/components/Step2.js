import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import { createOrderStep2, previous } from '../actions';
import data from '../data/dishes.json';

const renderSelectField = ({ input, label, type, meta: { touched, error }, children }) => (
  <div>
    <label>{label}</label>
    <div>
      <select {...input} className="select-area">
        {children}
      </select>
      {touched && error && <span>{error}</span>}
    </div>
  </div>
)
// console.log(data.dishes);

class FormStep2 extends Component {

  submitForm(values) {
    // event.preventDefault();
    this.props.createOrderStep2(values);
    
  }

  previousRedirect(value) {
    // console.log(value);
    this.props.previous(value);
  }
  
  
  render() {
    const { handleSubmit } = this.props;
    let arr = [];
    // console.log(arr);
    // console.log('state:', this.state);
    // console.log(this.props.orders);
    return(
      <form onSubmit={handleSubmit(this.submitForm.bind(this))}>
        <div className="form-group">
          <label>Please Select a Restaurant</label>
          <div>
            <Field name="restaurant" component={renderSelectField} required>
              <option value="">Select Option</option>
              {data.dishes.map(item => {
                  const selectItem = item.availableMeals.map((data, index) => {
                    // console.log(data);
                    if (data === this.props.orders.meal) {
                      arr.push(item.restaurant);
                      // console.log(arr);
                    }
                  })
                  return selectItem;
                })
              }
              {Array.from(new Set(arr)).map((name,index) => {
                return (
                    <option value={name} key={index}>{name}</option> 
                  )
                })
              }
            </Field>
          </div>
        </div>
        <div className="d-flex justify-content-between">
          <button type="button" className="btn btn-light" onClick={this.previousRedirect.bind(this, 'step1')}>Previous</button>
          <button type="submit" className="btn btn-primary">Next</button>
        </div>
      </form>
    );
  }
} 

function validate(values) {
  const errors = {};

  if (!values.restaurant){
    errors.restaurant = "Please Select!";
  }
  

  return errors;
}

function mapStateToProps(state) {
  return { orders: state.orders };
}

const mapPropsToDispatch = (dispatch) => {  
  return bindActionCreators({createOrderStep2, previous}, dispatch);
};

export default reduxForm({
  validate,
  form: 'FormStep2Form'
})(
connect(mapStateToProps, mapPropsToDispatch)(FormStep2)
); 