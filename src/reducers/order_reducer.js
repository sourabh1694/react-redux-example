import _ from "lodash";

import { CREATE_ORDER_STEP1, CREATE_ORDER_STEP2, CREATE_ORDER_STEP3, PREVIOUS } from '../actions';

const defaultState = {
  meal: '',
  people: '',
  redirect: 'step1',
  restaurant: '',
  dishes: [],
};


export default function(state = defaultState, action) {
  switch (action.type) {
    case CREATE_ORDER_STEP1: 
      // console.log(action);
      return {...state, meal:action.payload.meal, people: action.payload.people, redirect: 'step2'};
    case CREATE_ORDER_STEP2: 
      // console.log(action);
      return {...state, restaurant:action.payload.restaurant, redirect: 'step3'};
    case CREATE_ORDER_STEP3: 
      // console.log(action);
      let dish = action.payload;
      let newDish = { id: state.dishes.length + 1, name: dish.dish, quantity: dish.quantity};
      let newState = _.cloneDeep(state);
      newState.dishes.push(newDish);
      return newState;

    case PREVIOUS: 
      // console.log(action);
      return {...state, redirect:action.payload};
    default:
      return state;
  }
}