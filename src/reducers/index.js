import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import OrdersReducer from './order_reducer';

// Combine all reducers into one root reducer
export default combineReducers({
  orders: OrdersReducer,
  form:  formReducer
});