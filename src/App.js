import React, { Component } from 'react';
import './App.css';
import Step1 from './components/Step1';
import Step2 from './components/Step2';
import Step3 from './components/Step3';
import Review from './components/Review';
import { connect } from 'react-redux';

class App extends Component {
  render() {
    const { orders } = this.props;
    // console.log(orders.redirect);
    return (
      <div className="App">
        <ul className="nav justify-content-center bg-primary">
          <li>
            Test
          </li>
        </ul>
        <div className="btn-group">
          <button type="button" className={orders.redirect === 'step1' ? "btn btn-primary" : "btn btn-light"}>Step 1</button>
          <button type="button" className={orders.redirect === 'step2' ? "btn btn-primary" : "btn btn-light"}>Step 2</button>
          <button type="button" className={orders.redirect === 'step3' ? "btn btn-primary" : "btn btn-light"}>Step 3</button>
          <button type="button" className={orders.redirect === 'review' ? "btn btn-primary" : "btn btn-light"}>Review</button>
        </div>
        <div className="middle-content">
        { orders.redirect === 'step3' ? 
          <Step3 />
          : orders.redirect === 'step2' ? 
            <Step2 />
          : orders.redirect === 'review' ?
            <Review />
          :
            <Step1 />
        }
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { orders: state.orders };
}

export default connect(mapStateToProps,null)(App);
